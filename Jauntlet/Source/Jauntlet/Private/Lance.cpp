// Fill out your copyright notice in the Description page of Project Settings.

#include "Jauntlet.h"
#include "Lance.h"
#include "string.h"
#include "LanceMovementComponent.h"
#include "JauntletGameMode.h"
#include "JauntletCharacter.h"
#include "Sound/SoundCue.h"


// Sets default values
ALance::ALance()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	LanceMesh = CreateDefaultSubobject<USkeletalMeshComponent>( TEXT( "SK_Blade_BlackKnight" ) );
	RootComponent = LanceMesh;

	// Create an instance of our movement component, and tell it to update the root.
	MyMovementComponent = CreateDefaultSubobject<ULanceMovementComponent> ( TEXT ( "CustomMovementComponent" ) );
	MyMovementComponent->UpdatedComponent = RootComponent;

	// Collision
	this->OnActorHit.AddDynamic(this, &ALance::OnHit);
}

// Called when the game starts or when spawned
void ALance::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ALance::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
}

// Called to bind functionality to input
void ALance::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

}

void ALance::OnHit ( AActor* SelfActor, AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit )
{
	AGameMode* GameMode = GetWorld()->GetAuthGameMode(); // Get the GameMode
	AJauntletGameMode* JGameMode = Cast<AJauntletGameMode>( GameMode ); //downcast

    FVector f1(1200.f, -20.f, 270.f); //facing
    FVector f2(-1200.f, 20.f, 270.f);
    
    FRotator r1(180.f, 0.f, 0.f); //rotation
    FRotator r2(0.f, 0.f, 0.f);

	FTimerHandle timer;

	if ( hasCollided )
	{
		return;
	}

	if ( !JGameMode->GM_GetRoundFinished () )
	{
		if ( SelfActor->ActorHasTag ( "l1" ) && OtherActor->ActorHasTag ( "p2" ) )
		{
			// Player 1 landed a hit
			if ( JGameMode )
			{
				UE_LOG ( LogTemp, Error, TEXT ( "P1 hit P2" ) );
				hasCollided = true;
				JGameMode->GM_IncrementP1Wins ();
				// TODO:
				// Perform Ragdoll/Victory stuff
                
                DeathAC = PlayDeathSound(DeathSound);
                
				// Reset speed to 0
				mOwner->GetCharacterMovement()->MaxWalkSpeed = 1;
				AJauntletCharacter* otherPlayer = Cast<AJauntletCharacter>( OtherActor );
				otherPlayer->GetCharacterMovement()->MaxWalkSpeed = 1;

				// RESPAWN CHARACTERS
				//mOwner->SetActorRotation( r1 );
				//OtherActor->SetActorRotation( r2 );
				mOwner->SetActorLocation ( f1 );
				OtherActor->SetActorLocation ( f2 );
				SetActorRelativeLocation ( FVector ( -25.0f, 0.0f, 50.0f ) );
				otherPlayer->GetLance()->SetActorRelativeLocation ( FVector ( -25.0f, 0.0f, 50.0f ) );
				GetWorldTimerManager ().SetTimer (timer, this, &ALance::ResetCollision, 0.5f);
			}
		}
		else if ( SelfActor->ActorHasTag ( "l2" ) && OtherActor->ActorHasTag ( "p1" ) )
		{
			// Player 2 landed a hit
			if ( JGameMode )
			{
				UE_LOG ( LogTemp, Error, TEXT ( "P2 hit P1" ) );
				hasCollided = true;
				JGameMode->GM_IncrementP2Wins ();
				// TODO:
				// Perform Ragdoll/Victory stuff

                DeathAC = PlayDeathSound(DeathSound);
                
				// Reset speed to 0
				mOwner->GetCharacterMovement()->MaxWalkSpeed = 1;
				AJauntletCharacter* otherPlayer = Cast<AJauntletCharacter>( OtherActor );
				otherPlayer->GetCharacterMovement()->MaxWalkSpeed = 1;

				// RESPAWN CHARACTERS
				//mOwner->SetActorRotation( r2 );
				//OtherActor->SetActorRotation( r1 );
				mOwner->SetActorLocation ( f2 );
				OtherActor->SetActorLocation ( f1 );
				SetActorRelativeLocation ( FVector ( -25.0f, 0.0f, 50.0f ) );
				otherPlayer->GetLance()->SetActorRelativeLocation ( FVector ( -25.0f, 0.0f, 50.0f ) );
				GetWorldTimerManager ().SetTimer (timer, this, &ALance::ResetCollision, 0.5f);
			}
		}
	}
}

UPawnMovementComponent* ALance::GetMovementComponent() const
{
	return MyMovementComponent;
}

void ALance::MoveHorizontal ( float AxisValue )
{
	UE_LOG(LogTemp, Log, TEXT( "Moving Horizontal: %f" ), AxisValue );
	if (MyMovementComponent && (MyMovementComponent->UpdatedComponent == RootComponent))
	{
		SetActorLocation ( MyMovementComponent->GetActorLocation () + GetActorRightVector () * AxisValue * moveSpeed );
		//MyMovementComponent->AddInputVector(GetActorRightVector() * AxisValue);
	}
	//AddMovementInput ( GetActorRightVector(), AxisValue * moveSpeed );
}

void ALance::MoveVertical ( float AxisValue )
{
	UE_LOG(LogTemp, Log, TEXT( "Moving Vertical: %f" ), AxisValue );
	if (MyMovementComponent && (MyMovementComponent->UpdatedComponent == RootComponent))
	{
		MyMovementComponent->AddInputVector(GetActorUpVector() * AxisValue);
	}
	//AddMovementInput ( GetActorUpVector(), AxisValue * moveSpeed );
}

UAudioComponent* ALance::PlayDeathSound(USoundCue* Sound)
{
    UAudioComponent* AC = NULL;
    if(Sound)
    {
        AC = UGameplayStatics::SpawnSoundAttached(Sound, RootComponent);
    }
    return AC;
}
