// Fill out your copyright notice in the Description page of Project Settings.
#include "Jauntlet.h"
#include "JauntletPlayerController.h"
#include "JauntletCharacter.h"

AJauntletPlayerController::AJauntletPlayerController()
{
    bShowMouseCursor = false;
}

void AJauntletPlayerController::BeginPlay()
{
    Super::BeginPlay();

}

void AJauntletPlayerController::PlayerTick(float DeltaTime)
{
    Super::PlayerTick(DeltaTime);
    
}

void AJauntletPlayerController::SetupInputComponent()
{
    Super::SetupInputComponent();
    
    InputComponent->BindAction("Jump", IE_Pressed, this, &AJauntletPlayerController::Jump);
    InputComponent->BindAction("Jump", IE_Released, this, &AJauntletPlayerController::StopJumping);
    
    InputComponent->BindAxis("MoveForward", this, &AJauntletPlayerController::MoveForward);
    InputComponent->BindAxis("MoveRight", this, &AJauntletPlayerController::MoveRight);
    
    InputComponent->BindAxis("Turn", this, &AJauntletPlayerController::AddControllerYawInput);
    InputComponent->BindAxis("TurnRate", this, &AJauntletPlayerController::TurnAtRate);
    InputComponent->BindAxis("LookUp", this, &AJauntletPlayerController::AddControllerPitchInput);
    InputComponent->BindAxis("LookUpRate", this, &AJauntletPlayerController::LookUpAtRate);
}

void AJauntletPlayerController::Jump()
{
    auto character = Cast<ACharacter>(GetPawn());
    
    if(character)
    {
        //should add dead checks later
        character->Jump();
    }
    
}

void AJauntletPlayerController::StopJumping()
{
    auto character = Cast<ACharacter>(GetPawn());
    
    if(character)
    {
        //should add dead checks later
        character->StopJumping();
    }
}

void AJauntletPlayerController::MoveForward(float Val)
{
    auto character = Cast<AJauntletCharacter>(GetPawn());
    
    if(character)
    {
        character->MoveForward(Val);
    }
}

void AJauntletPlayerController::MoveRight(float Val)
{
    auto character = Cast<AJauntletCharacter>(GetPawn());
    
    if(character)
    {
        character->MoveRight(Val);
    }
}

void AJauntletPlayerController::AddControllerYawInput(float Val)
{
    auto Pawn = GetPawn();
    if(Pawn)
    {
        Pawn->AddControllerYawInput(Val);
    }
}

void AJauntletPlayerController::TurnAtRate(float Rate)
{
    auto character = Cast<AJauntletCharacter>(GetPawn());
    if(character)
    {
        character->TurnAtRate(Rate);
    }
}

void AJauntletPlayerController::AddControllerPitchInput(float Val)
{
    auto Pawn = GetPawn();
    if(Pawn)
    {
        Pawn->AddControllerPitchInput(Val);
    }
}

void AJauntletPlayerController::LookUpAtRate(float Rate)
{
    auto character = Cast<AJauntletCharacter>(GetPawn());
    if(character)
    {
        character->LookUpAtRate(Rate);
    }
}

